package test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestInitialization {
	public static WebDriver driver;

	public void startSession() {
		/*System.setProperty("webdriver.gecko.driver",
				"/Users/Abrar/Downloads/geckodriver");
		driver = new FirefoxDriver();*/
		System.setProperty("webdriver.chrome.driver",
				"/Users/Abrar/Desktop/chromedriver");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	}
//
	public void closeSession() {
		driver.quit();
	}
}
