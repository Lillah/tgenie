package test;

import java.util.Iterator;

import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.apache.poi.ss.formula.functions.Count;
import org.apache.poi.xssf.usermodel.*;

import java.io.FileInputStream;
import java.io.IOException;

import common.CommonObject;

public class genie extends TestInitialization {
	
	CommonObject cmnObj = new CommonObject();
	PageObject pgObj = new PageObject();
	traceGeniePageObject tgPgObj = new traceGeniePageObject();

	@BeforeMethod
	@Override
	public void startSession(){
		super.startSession();
	}
	
	@AfterMethod
	@Override
	public void closeSession(){
		super.closeSession();
	}
	
	String path;
    public FileInputStream fis = null;
    private XSSFWorkbook workbook = null;
    private XSSFSheet sheet = null;
    private XSSFRow row   =null;
    private XSSFCell cell = null;
	
	@Test
	public void traceGenieSearch() throws Exception{
		try{
			
			String url = "http://www.tracegenie.com/amember4/amember/1DAY/searcharea.html";
			int count = 0;
			int serialNumber = 0;
			String streetName;
			
			// Retrieve the values from the spreadsheet and enter on trace genie
			String path = System.getProperty ("user.home") + "/Desktop/Addresses of RG1,2,4,5,6,8,14,30,31.xlsx";
	        fis = new FileInputStream(path); 
	        workbook = new XSSFWorkbook(fis);
	        sheet = workbook.getSheetAt(0);
	        
	        int index = workbook.getSheetIndex("RG4");
	        sheet = workbook.getSheetAt(index);
	        //int rownumber=sheet.getLastRowNum()+1; //uncomment this to run code on all the rows of the sheet
	        int startRownumber=500;
	        int endRownumber=549;
	        
	        driver.get(url);
			cmnObj.cmnWaitFluentlyForElement(tgPgObj.initElements().streetName, 20);
			
			//TODO: Write the login code here, currently needs to put breakpoint in the above line and login with credentials   
			
			tgPgObj.initElements().cityName.clear();
			Thread.sleep(2000);
			tgPgObj.initElements().cityName.sendKeys("READING");
			Thread.sleep(2000);
	        
			for (int i = startRownumber; i < endRownumber; i++) {
				row = sheet.getRow(i);
				//int colnumber = row.getLastCellNum();

				cell = row.getCell(2);
				//System.out.println(cell.getStringCellValue() + "\n");
				streetName = cell.getStringCellValue();

				//Enter the street name and area and click on Search
				driver.switchTo().defaultContent();
				tgPgObj.initElements().streetName.clear();
				Thread.sleep(2000);
				tgPgObj.initElements().streetName.sendKeys(streetName);
				Thread.sleep(2000);
				tgPgObj.initElements().searchButton.click();
				Thread.sleep(15000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.className("style24frameb")));

				// Wait for the search results
				//cmnObj.cmnWaitFluentlyForElement(tgPgObj.initElements().results, 20);
				
				// TODO: Copy and Print all the listed names and address
				int numberOfResults = tgPgObj.initElements().numberOfResults.size();
				if (numberOfResults > 0) {
					do {
						
						for (int k = 0; k < numberOfResults; k++) {
							int resultNumber = k + 1;
							String name = driver.findElement(By.xpath("(//table[@class='table table-hover table-curved']//tr[@class='info']//h5 [text()])["+ resultNumber +"]")).getText().toString();
							String address = driver.findElement(By.xpath("(//table[@class='table table-hover table-curved']//tr[@class='success']//h5 [text()])["+ resultNumber +"]")).getText().toString();
							
							serialNumber = ++count;
							System.out.print(serialNumber + ", " + streetName + ", " + name + ", " + address + "\n");
							Thread.sleep(1000);

						}
						//Check if Next is available and click in it
						if (!tgPgObj.initElements().nextPages.isEmpty()) {
							tgPgObj.initElements().nextPage.click();
							Thread.sleep(5000);
						}
						
						
					} while (!tgPgObj.initElements().nextPages.isEmpty());
				}
				
				//For the last page without Next button
				if (!tgPgObj.initElements().prevPages.isEmpty()) {
					int numberOfResultsOnLastPage = tgPgObj.initElements().numberOfResults.size();
					for (int k = 0; k < numberOfResultsOnLastPage; k++) {
						int resultNumber = k + 1;
						String name = driver.findElement(By.xpath("(//table[@class='table table-hover table-curved']//tr[@class='info']//h5 [text()])["+ resultNumber +"]")).getText().toString();
						String address = driver.findElement(By.xpath("(//table[@class='table table-hover table-curved']//tr[@class='success']//h5 [text()])["+ resultNumber +"]")).getText().toString();
						
						serialNumber = ++count;
						System.out.print(serialNumber + ", " + streetName + ", " + name + ", " + address + "\n");
						Thread.sleep(1000);

					}
					
				}
				
			}
			
			//Print to the spreadsheet
			//TODO:  Try separating post codes
			
		} catch (Exception e) {
			System.out.print(e);
			throw new Exception(e);
		}
		
		
	}
}
