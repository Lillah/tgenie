package test;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class traceGeniePageObject extends TestInitialization {
	
	@FindBy(xpath="//span[text()='Your Account']")
	WebElement landingPage;
	
	@FindBy(id="twotabsearchtextbox")
	WebElement searchBox;
	
	@FindBy(xpath="//h4[text()='Street search']/..//input[@name='q6']")
	WebElement streetName;
	
	@FindBy(xpath="//h4[text()='Street search']/..//input[@name='q422']")
	WebElement cityName;
	
	//@FindBy(xpath="//input[@type='submit']")
	@FindBy(xpath="//h4[text()='Street search']/..//input[@alt='Search']")
	WebElement searchButton;
	
	@FindBy(xpath="//font[contains(text(), 'Showing results')]")
	WebElement results;
	
	@FindBy(xpath="//table[@class='table table-hover table-curved']")
	List<WebElement> numberOfResults;
	
	@FindBy(xpath="//a[@class='a-size-small a-link-normal a-text-normal' and contains(@href, 'customerReviews')]")
	List<WebElement> numberOfCustomerReviews;
	
	@FindBy(xpath="//font[text()='Next 50 >>']")
	WebElement nextPage;
	
	@FindBy(xpath="//font[text()='Next 50 >>']")
	List<WebElement> nextPages;
	
	@FindBy(xpath="//font[text()='Prev 50']")
	List<WebElement> prevPages;
	
	public traceGeniePageObject initElements(){
		return PageFactory.initElements(driver, traceGeniePageObject.class);
	}

}
